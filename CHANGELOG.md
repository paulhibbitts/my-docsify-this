# Changelog

## XX/XX/XXXX

**Improved:**  
* Updated ReadMe
* Loading min.js versions of resources
* Improved user interface of URL Builder
* Initial support for alternative ToC CSS (`toc-alt`)
* Support for display of standard Docsify Sidebar
* Updated Themeable and support of light/dark theme switching (OS-level)
* Changed from static Docsify URL to dynamic (i.e. current URL)
* Restored CDN loading of scripts
* Added support to retrieve values of URL parms (i.e. "Edit this Page" link)

**Bugfix:**  
* Added missing source maps

## 07/22/2022

**New:**  
* Welcome to the public preview of Docsify-This!
